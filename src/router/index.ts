import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
import type {RouteRecordRaw} from "vue-router"
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/index2',
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('@/views/HomeView.vue'),
    children:[
      {
        path: '/index',
        name: 'index',
        component: () => import('@/views/index/index.vue'),
      },
      {
        path: '/index2',
        name: 'index2',
        component: () => import('@/views/index/index2.vue'),
      },
      {
        path: '/index3',
        name: 'index3',
        component: () => import('@/views/index/index3.vue'),
      },
      {
        path: '/index4',
        name: 'index4',
        component: () => import('@/views/index/index4.vue'),
      },
      {
        path: '/index5',
        name: 'index5',
        component: () => import('@/views/index/index5.vue'),
      }
    ]
  },
]
const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes,
})

router.beforeEach((to, from, next) => {
  next();
})

export default router
