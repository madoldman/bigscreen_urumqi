export default {
    'industry':'/api/lsd/industry/situation',
    'economy':'/api/lsd/economy/operation',
    'markey_entity':'/api/lsd/markey/entity',
    'graph':'/api/lsd/industry/graph',
    'markey_list': '/api/lsd/markey/entity/list',
    'position':'/api/lsd/markey/entity/position',
    'ent':'/api/lsd/industry/ent',
    'position2':'/api/lsd/industry/graph/position',
    'chain':'/api/lsd/industry/graph/chain',
    'login':'/api/lsd/login',
    'chat':'/api/chat/chat',
    'config':'/api/chat/template/config',
}