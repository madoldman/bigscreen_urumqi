import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import BaiduMap from 'vue-baidu-map-3x'



import '@/assets/css/main.scss'
import '@/assets/css/tailwind.css'

import {registerEcharts} from "@/plugins/echarts"
//不使用mock 请注释掉
import { mockXHR } from "@/mock/index";
mockXHR()

const app = createApp(App)
registerEcharts(app)
app.use(createPinia())
app.use(router)
app.use(BaiduMap,{
   ak: '53iVPwfk6nMg9969AT6GLLokqEYU5qoO',
   dragging: true,
});
app.mount('#app')
